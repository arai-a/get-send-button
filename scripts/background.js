(async () =>  {

  // This must be done before optionsMigrate, because of still existing and used prefs in optionsMigrate
  messenger.WindowListener.registerDefaultPrefs("defaults/preferences/getsendbutton.js");
  
  await optionsMigrate();
  await optionsInit();
  await messenger.storage.onChanged.addListener(optionsInit);

  // Add eventListener for onClicked on the toolbar button
  /*
  browser.browserAction.onClicked.addListener(onToolbarButtonClicked);
  async function onToolbarButtonClicked(tab, info) {
    consoleDebug("GetSendButton: browserAction.onClicked: Toolbar button fired");

    gsbFunctions.AllowHTMLtemp(tab.id, info.modifiers, false);
  }
  */


  /* ************************* WL API part below ****************************** */

  messenger.WindowListener.registerChromeUrl([ 
    ["content",   "getsendbutton",          "chrome/content/"],
    ["resource",  "getsendbutton",          "chrome/"],
  ]);

  messenger.WindowListener.registerWindow(
    "chrome://messenger/content/customizeToolbar.xhtml", 
    "chrome://getsendbutton/content/getsendbutton_overlay_customizeToolbar.js");

  messenger.WindowListener.registerWindow(
    "chrome://messenger/content/messageWindow.xhtml", 
    "chrome://getsendbutton/content/getsendbutton_overlay.js");

  messenger.WindowListener.registerWindow(
    "chrome://messenger/content/messenger.xhtml", 
    "chrome://getsendbutton/content/getsendbutton_overlay.js");

  // messenger.WindowListener.registerShutdownScript("chrome://getsendbutton/content/shutdown.js")

  messenger.WindowListener.startListening();

})();