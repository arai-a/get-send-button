function saveOptions(e) {
  e.preventDefault();

  return messenger.storage.local.set({
    debug: document.getElementById("debug").checked,
    sendYes: document.querySelector('input[name=buttonSendOrGetAndSend]:checked').value,
    onlySingleAccount: document.querySelector('input[name=buttonSendSingleVsAll]:checked').value,
    askPasswords: document.getElementById("askPasswords").checked,
  });
}

function restoreOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    consoleDebug("GetSendButton: restoreOption(id): option.id = " + id);
    let element="";
    switch(id) {
      case "sendYes":
        // radio handling
        element = document.getElementById(res[id] || DefaultOptions[id]);
        element.checked = true;
        break;
      case "onlySingleAccount":
        // radio handling
        element = document.getElementById(res[id] || DefaultOptions[id]);
        element.checked = true;
        break;
      default:
        // checkbox handling
        element = document.getElementById(id);
        if (element.type && element.type == "checkbox") {
          consoleDebug( "GetSendButton: restoreOption(id): " + element.id + " = " + res[id] );
          if (res[id] === undefined) {
            element.checked = DefaultOptions[id];
          } else {
            element.checked = res[id];
          }
        } else {
          consoleDebug( "GetSendButton: restoreOption(id): " + element.id + " = " + res[id] );
          element.value = res[id] || DefaultOptions[id];
        }
    }
  }, defaultError);
}

async function restoreAllOptions() {
  await restoreOption("debug");
  await restoreOption("sendYes");
  await restoreOption("onlySingleAccount");
  await restoreOption("askPasswords");
}

async function resetToDefault() {
  // Reset localstorage options
  return messenger.storage.local.remove(OptionsList).then(() => {
    restoreAllOptions();
  });
}
