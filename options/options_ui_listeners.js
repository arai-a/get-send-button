// onLoad listener to load Options and LegacyPrefs
document.addEventListener('DOMContentLoaded', async () => {
  consoleDebug("GetSendButton: DOMContentLoaded, restoreAllOptions()");
  restoreAllOptions();
});

// onChange listener for all options to save the changed options
OptionsList.forEach((option) => {
  consoleDebug("GetSendButton: OptionsList.forEach(): option: " + option);
  switch(option) {
    case "sendYes":
      document.getElementById("sendYes").addEventListener("change", (e) => {
        consoleDebug("GetSendButton: OptionsList.forEach():  Option buttonSendOrGetAndSend changed, saveOptions()");
        saveOptions(e);
      });
      document.getElementById("sendNo").addEventListener("change", (e) => {
        consoleDebug("GetSendButton: OptionsList.forEach():  Option buttonSendOrGetAndSend changed, saveOptions()");
        saveOptions(e);
      });
      break;

    case "onlySingleAccount":
      document.getElementById("getSingle").addEventListener("change", (e) => {
        consoleDebug("GetSendButton: OptionsList.forEach():  Option buttonSendSingleVsAll changed, saveOptions()");
        saveOptions(e);
      });
      document.getElementById("getAll").addEventListener("change", (e) => {
        consoleDebug("GetSendButton: OptionsList.forEach():  Option buttonSendSingleVsAll changed, saveOptions()");
        saveOptions(e);
      });
      break;

    default:
      consoleDebug("GetSendButton: OptionsList.forEach():  register Listener for Option " + option + "");
      document.getElementById(option).addEventListener("change", (e) => {
        consoleDebug("GetSendButton: OptionsList.forEach():  Option " + option + " changed, saveOptions()");
        saveOptions(e);
        // .then(() => { enableOrDisableOptionUiElements(); });
      });
  }
});

// reset click listener
document.getElementById("resetToDefault").addEventListener("click", () => {
  consoleDebug("GetSendButton: Reset clicked, resetToDefault()");
  resetToDefault();
});
