Preferences.addAll([
  {
    id: "extensions.getsendbutton.GetSendButton_SendYes",
    type: "bool"
  },
  {
    id: "extensions.getsendbutton.GetSendButton_OnlySingleAccount",
    type: "bool"
  },
  {
    id: "extensions.getsendbutton.GetSendButton_AskPasswords",
    type: "bool"
  }
]);