// Import any needed modules.
var { Services } = ChromeUtils.import(
  "resource://gre/modules/Services.jsm"
);

// Load an additional JavaScript file.
Services.scriptloader.loadSubScript("chrome://getsendbutton/content/getsendbutton_functions.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://getsendbutton/content/getsendbutton_buttonListeners.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://getsendbutton/content/getsendbutton_prefsListener.js", window, "UTF-8");

function onLoad(activatedWhileWindowOpen) {
  
  WL.injectCSS("resource://getsendbutton/skin/getsendbutton.css");
  WL.injectElements(`
  <toolbarpalette id="MailToolbarPalette">
    <toolbarbutton
      id="button-getsendbutton" 
      label="__MSG_buttonGetAndSend__"
      label-getMsg="__MSG_buttonGetOnly__" 
      label-getAndSend="__MSG_buttonGetAndSend__"
      class="toolbarbutton-1"
      is="toolbarbutton-menu-button"
      type="menu-button"
      oncommand="GetSendButton_functions.GetSingleAccountOrGetAndSend(event.target._folder,event);">
      <menupopup id="button-getsendbutton-popup" 
        onpopupshowing="file_init();">
        <menu id="GetSendButton_messenger_menupopup_ReceiveAccount" 
          class="menuitem-iconic" 
          label="&getNewMsgForCmd.label;" 
          tooltiptext="&getNewMsgForCmd.label;">
          <menupopup onpopupshowing="getMsgToolbarMenu_init();" 
            is="folder-menupopup" 
            expandFolders="false" 
            mode="getMail">
            <menuitem id="GetSendButton_messenger_menupopup_ReceiveAll" 
              class="menuitem-iconic" 
              label="__MSG_menuGetall__" 
              tooltiptext="__MSG_menuGetall_tooltip__" 
              oncommand="GetSendButton_functions.GetMessagesForAllAccounts();event.stopPropagation();" />
            <menuitem id="GetSendButton_messenger_menupopup_ReceiveAuth" 
              class="menuitem-iconic" 
              label="__MSG_menuGetauth__" 
              tooltiptext="__MSG_menuGetauth_tooltip__" 
              oncommand="goDoCommand('cmd_getMsgsForAuthAccounts');event.stopPropagation();" />
            <menuseparator id="button-getAllNewMsgSeparator"/>
          </menupopup>
        </menu>
        <menuitem id="GetSendButton_messenger_menupopup_sendUnsent" 
          class="menuitem-iconic" 
          label="__MSG_menuSendall__" 
          tooltiptext="__MSG_menuSendall_tooltip__" 
          oncommand="goDoCommand('cmd_sendUnsentMsgs');event.stopPropagation();">
        </menuitem>
        <menuseparator/>
        <menuitem id="GetSendButton_messenger_menupopup_SyncAll" 
          class="menuitem-iconic" 
          label="&synchronizeOfflineCmd.label;" 
          tooltiptext="__MSG_menuSyncAll_tooltip__" 
          oncommand="goDoCommand('cmd_synchronizeOffline');event.stopPropagation();">
        </menuitem>
        <menuitem id="GetSendButton_messenger_menupopup_SyncFlagged" 
          class="menuitem-iconic" 
          label="&downloadStarredCmd.label;" 
          oncommand="goDoCommand('cmd_downloadFlagged');event.stopPropagation();">
        </menuitem>
        <menuitem id="GetSendButton_messenger_menupopup_SyncSelected" 
          class="menuitem-iconic" 
          label="&downloadSelectedCmd.label;" 
          oncommand="goDoCommand('cmd_downloadSelected');event.stopPropagation();">
        </menuitem>
      </menupopup>
    </toolbarbutton>
  </toolbarpalette>

  <toolbarpalette id="MailToolbarPalette">
    <toolbarbutton id="GetSendButton_S_all" 
      class="toolbarbutton-1" 
      label="__MSG_buttonSendall__" 
      tooltiptext="__MSG_buttonSendall_tooltip__" 
      oncommand="goDoCommand('cmd_sendUnsentMsgs')">
    </toolbarbutton>
  </toolbarpalette>
  `, ["chrome://messenger/locale/messenger.dtd"]);

  window.GetSendButton_status.startup();
  window.GetSendButton_label.startup();

}

function onUnload(deactivatedWhileWindowOpen) {

  window.GetSendButton_status.shutdown();
  window.GetSendButton_label.shutdown();

}
